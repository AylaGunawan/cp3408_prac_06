using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentManager : MonoBehaviour
{
    List<NavMeshAgent> agents = new List<NavMeshAgent>();

    // Start is called before the first frame update
    void Start()
    {
        // make a list of nav mesh agent components from ai game objects
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("AI");
        foreach (GameObject go in gameObjects)
        {
            agents.Add(go.GetComponent<NavMeshAgent>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // cast ray from mouse pos on screen to perspective position in space for dist 100
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
            {
                foreach (NavMeshAgent a in agents)
                {
                    a.SetDestination(hit.point);
                }
            }
        }
    }
}
